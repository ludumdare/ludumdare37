﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class WorkBootstrap : MonoBehaviour
{
    public string _startingScene;

    private void Start()
    {
        SceneManager.LoadScene(_startingScene);
    }
}
