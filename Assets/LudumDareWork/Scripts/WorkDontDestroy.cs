﻿using UnityEngine;
using System.Collections;

public class WorkDontDestroy : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
