﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class WorkGlobalMenu : MonoBehaviour
{
    public void MainMenu()
    {
        SceneManager.LoadScene("10-WorkMenu");
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
