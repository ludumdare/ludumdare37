﻿using UnityEngine;
using System.Collections;

public class WorkCameraMenu : MonoBehaviour
{
    public int baseWidth = 640;
    public int baseHeight = 360;
    public void FullScreen()
    {
        Screen.SetResolution(baseWidth*3, baseHeight*3, true);
    }

    public void NormalScreen()
    {
        Screen.SetResolution(baseWidth, baseHeight, false);
    }

    public void DoubleScreen()
    {
        Screen.SetResolution(baseWidth*2, baseHeight*2, false);
    }

    private void Start()
    {
        NormalScreen();
    }
}
