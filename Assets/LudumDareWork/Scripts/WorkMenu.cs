﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class WorkMenu : MonoBehaviour
{
    public void OpenScene(string name)
    {
        SceneManager.LoadScene(name);
    }

}
