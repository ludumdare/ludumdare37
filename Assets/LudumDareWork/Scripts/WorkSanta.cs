﻿using UnityEngine;
using System.Collections;

using LudumDare.Managers;
using LudumDare.Game;

public class WorkSanta : MonoBehaviour
{
    public SantasBagObject _santasBag;
    public int _presentsNeeded;

    public void AddPresent()
    {
        GameObject go =  GameManager.Instance.PrefabFactory.CreatePrefab("Present");
        int x = Random.Range(40, 180);
        int y = Random.Range(30, 150);
        go.transform.position = new Vector2(x, y);
        go.GetComponent<PresentObject>().Collect();
    }

    private void Start()
    {
        GameManager.Instance.GameController.PresentsNeeded = _presentsNeeded;
        GameManager.Instance.GameController.PresentsCollected = 0;
    }

}
