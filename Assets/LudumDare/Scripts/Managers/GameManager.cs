﻿using UnityEngine;
using System.Collections;

using LudumDare.Data;
using LudumDare.Controllers;
using LudumDare.Factories;

namespace LudumDare.Managers
{
    public class GameManager : MonoSingleton<GameManager>
    {
        public GameSettingsData GameSettings { get; set; }

        public GameController GameController { get; set; }
        public SoundController SoundController { get; set; }
        public MusicController MusicController { get; set; }
        public PrefabFactory PrefabFactory { get; set; }
        public InputController InputController { get; set; }
        public ScoreController ScoreController { get; set; }
        //public GuiController GuiController { get; set; }
        public ConveyorController ConveyorController { get; set; }
        public RoomController RoomController { get; set; }
        public CheatController CheaterController { get; set; }

    }

    
}
