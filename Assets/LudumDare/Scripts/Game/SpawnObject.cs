﻿using UnityEngine;
using System.Collections;

namespace LudumDare.Game
{
    public enum SpawnTypes
    {
        SPAWN_NONE,
        SPAWN_PRESENT,
        SPAWN_EMENY,
        SPAWN_POWERUP
    }
    public class SpawnObject : MonoBehaviour
    {
        public System.Guid SpawnId {get;set;}
        public SpawnTypes SpawnType { get; set; }

        public SpawnObject()
        {
            SpawnId = System.Guid.NewGuid();
            SpawnType = SpawnTypes.SPAWN_NONE;
        }
    }
}

