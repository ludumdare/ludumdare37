﻿using UnityEngine;
using System.Collections;

using LudumDare.Brushes;

namespace LudumDare.Game
{
    public class PathNode
    {
        public PathBrush NodeUp;
        public PathBrush NodeDown;
        public PathBrush NodeLeft;
        public PathBrush NodeRight;
    }
}
