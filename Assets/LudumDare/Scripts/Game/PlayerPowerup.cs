﻿using UnityEngine;

using System.Collections;

using Sdn.Core;
using Sdn.SpriteBoss;

using LudumDare.Data;
using LudumDare.Events;
using LudumDare.Managers;

namespace LudumDare.Game
{
    public class PlayerPowerup : MonoBehaviour
    {
        private PowerupData _powerup;
        private Coroutine _runPowerup;
        private PlayerMover _playerMover;

        private SpriteRenderer _icon;

        private void onPowerupEvent(PowerupEvent e)
        {
            D.Trace("[PlayerPowerup] onPowerupEvent( e: {0} )", e);
            //  TODO: put effect on player

            string id = e.Data.Id;

            if (_powerup != null)
            {
                //  TODO: handle when powerup is already running
                return;
            }

            GameManager.Instance.SoundController.PlaySound("Powerup");
            _runPowerup = StartCoroutine(runPowerup(e.Data));
            return;

        }

        private IEnumerator runPowerup(PowerupData data)
        {
            D.Trace("[PlayerPowerup] runPowerup( data: {0} )", data);

            _powerup = data;

            D.Detail("- starting powerup");

            //  put icon above player
            _icon.sprite = _powerup.Icon;
            _icon.enabled = true;

            if (data.Id == "STOCKING")
            {
                yield return StartCoroutine(runStockingEffect());
            }

            if (data.Id == "MAGNET")
            {
                yield return StartCoroutine(runMagnetEffect());
            }

            if (data.Id == "STAR")
            {
                yield return StartCoroutine(runStarPowerup());
            }

            if (data.Id == "CLOCK")
            {
                yield return StartCoroutine(runClockPowerup());
            }

            if (data.Id == "CANDY")
            {
                yield return StartCoroutine(runCandyPowerup());
            }

            //  disable icon
            _icon.enabled = false;

            _powerup = null;

            D.Detail("- ending powerup");

            yield return null;
        }

        private IEnumerator runCandyPowerup()
        {
            D.Trace("[PlayerPowerup] runCandyPowerup( data: {0} )", _powerup);

            D.Detail("- starting clock powerup effect");
            GameManager.Instance.GameController.Magic += 1;

            yield return null;
        }

        private IEnumerator runClockPowerup()
        {
            D.Trace("[PlayerPowerup] runClockPowerup( data: {0} )", _powerup);

            D.Detail("- starting clock powerup effect");

            SimpleEvents.Instance.Raise(new EffectStartedEvent
            {
                Data = _powerup,
            });

            SimpleEvents.Instance.Raise(new PauseEvent());

            yield return new WaitForSeconds(_powerup.Duration - 2.0f);
            StartCoroutine(flashIcon());
            yield return new WaitForSeconds(2.0f);

            SimpleEvents.Instance.Raise(new ResumeEvent());

            D.Detail("- ending clock powerup effect");

            SimpleEvents.Instance.Raise(new EffectEndedEvent
            {

            });

            yield return null;
        }

        private IEnumerator runStarPowerup()
        {
            D.Trace("[PlayerPowerup] runStarPowerup( data: {0} )", _powerup);

            D.Detail("- starting star powerup effect");

            SimpleEvents.Instance.Raise(new EffectStartedEvent
            {
                Data = _powerup,
            });

            StartCoroutine(flashIcon());

            foreach (PresentObject present in GameObject.FindObjectsOfType<PresentObject>())
            {
                present.Collect();
            }

            D.Detail("- ending star powerup effect");

            SimpleEvents.Instance.Raise(new EffectEndedEvent
            {

            });

            yield return null;
        }

        private IEnumerator runMagnetEffect()
        {
            D.Trace("[PlayerPowerup] runMagnetEffect( data: {0} )", _powerup);

            D.Detail("- starting magnet powerup effect");

            SimpleEvents.Instance.Raise(new EffectStartedEvent
            {
                Data = _powerup,
            });

            BoxCollider2D box = GetComponent<BoxCollider2D>();
            Vector2 size = box.size;
            box.size = new Vector2(32, 24);

            yield return new WaitForSeconds(_powerup.Duration - 2.0f);
            StartCoroutine(flashIcon());
            yield return new WaitForSeconds(2.0f);

            box.size = size;

            D.Detail("- ending magnet powerup effect");

            SimpleEvents.Instance.Raise(new EffectEndedEvent
            {

            });

            yield return null;
        }

        private IEnumerator runStockingEffect()
        {
            D.Trace("[PlayerPowerup] runStockingEffect( data: {0} )", _powerup);

            D.Detail("- starting stocking powerup effect");

            SimpleEvents.Instance.Raise(new EffectStartedEvent
            {
                Data = _powerup,
            });

            float speed = _playerMover.Speed;
            _playerMover.Speed = speed * 2;
            _playerMover.IsSpeedy = true;

            yield return new WaitForSeconds(_powerup.Duration - 2.0f);
            StartCoroutine(flashIcon());
            yield return new WaitForSeconds(2.0f);

            _playerMover.Speed = speed;
            _playerMover.IsSpeedy = false;

            D.Detail("- ending stocking powerup effect");

            SimpleEvents.Instance.Raise(new EffectEndedEvent
            {

            });

            yield return null;
        }

        private IEnumerator flashIcon()
        {
            for(int i = 0; i < 6; i++)
            {
                _icon.enabled = false;
                yield return new WaitForSeconds(0.06f);
                _icon.enabled = true;
                yield return new WaitForSeconds(0.14f);
            }
        }

        private void Awake()
        {
            D.Trace("[PlayerPowerup] Awake");
            SimpleEvents.Instance.AddListener<PowerupEvent>(onPowerupEvent);
            _playerMover = GetComponent<PlayerMover>();
            _icon = transform.Find("Icon").GetComponent<SpriteRenderer>();
        }

        private void OnDestroy()
        {
            SimpleEvents.Instance.RemoveListener<PowerupEvent>(onPowerupEvent);
        }
    }
}
