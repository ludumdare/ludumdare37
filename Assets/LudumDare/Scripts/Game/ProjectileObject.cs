﻿using UnityEngine;
using System.Collections;

using Sdn.Core;

using LudumDare.Events;
using LudumDare.Managers;

namespace LudumDare.Game
{
    public class ProjectileObject : MonoBehaviour
    {

        private void OnTriggerEnter2D(Collider2D collision)
        {
            D.Trace("[ProjectileObject] OnTriggerEnter2D");
            if (collision.tag == "Enemy")
            {
                GameManager.Instance.SoundController.PlaySound("EnemyHit");
                EnemyObject enemy = collision.GetComponent<EnemyObject>();
                PresentObject present = enemy.Present;

                if (present != null)
                {
                    present.transform.parent = null;
                    present.Collect();
                }

                Destroy(collision.gameObject);

                SimpleEvents.Instance.Raise(new ScoreEvent
                {
                    Points = 100,
                    Position = transform.position,
                    From = "Projectile",
                    Animate = false,
                });
            }
        }
    }
}