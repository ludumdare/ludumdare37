﻿using UnityEngine;

using Sdn.Core;
using Sdn.SpriteBoss.Modules;

using LudumDare.Events.Input;

namespace LudumDare.Game
{

    public class PlayerListener : SpriteBossModule
    {
        private PlayerMover _mover;

        public override void OnModuleInit()
        {
            D.Trace("[InputListener] OnModuleInit");
            base.OnModuleInit();
            SimpleEvents.Instance.AddListener<InputRightEvent>(onMoveRight);
            SimpleEvents.Instance.AddListener<InputLeftEvent>(onMoveLeft);
            SimpleEvents.Instance.AddListener<InputUpEvent>(onMoveUp);
            SimpleEvents.Instance.AddListener<InputDownEvent>(onMoveDown);
            SimpleEvents.Instance.AddListener<InputFireEvent>(onFire);
        }

        private void onMoveRight( InputRightEvent e)
        {
            D.Trace("[InputListener] onMoveRight");

            _mover.MoveRight();
        }

        private void onMoveLeft(InputLeftEvent e)
        {
            D.Trace("[InputListener] onMoveLeft");

            _mover.MoveLeft();
        }

        private void onMoveUp(InputUpEvent e)
        {
            D.Trace("[InputListener] onMoveUp");

            _mover.MoveUp();
        }

        private void onMoveDown(InputDownEvent e)
        {
            D.Trace("[InputListener] onMoveDown");

            _mover.MoveDown();
        }

        private void onFire(InputFireEvent e)
        {
            if (e.FireLeft)
            {
                _mover.FireLeft();
            }
            else
            {
                _mover.FireRight();
            }
        }

        private void Awake()
        {
            _mover = GetComponent<PlayerMover>();
        }

        private void OnDestroy()
        {
            SimpleEvents.Instance.RemoveListener<InputRightEvent>(onMoveRight);
            SimpleEvents.Instance.RemoveListener<InputLeftEvent>(onMoveLeft);
            SimpleEvents.Instance.RemoveListener<InputUpEvent>(onMoveUp);
            SimpleEvents.Instance.RemoveListener<InputDownEvent>(onMoveDown);
            SimpleEvents.Instance.RemoveListener<InputFireEvent>(onFire);
        }
    }
}
