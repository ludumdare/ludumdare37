﻿using UnityEngine;
using System.Collections;

using Sdn.Core;

using LudumDare.Managers;
using LudumDare.Events.Input;

namespace LudumDare.Game
{
    public class PlayerEnemyCollider : MonoBehaviour
    {
        private PlayerMover _mover;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Enemy")
            {
                GameManager.Instance.SoundController.PlaySound("PlayerHit");

                if (_mover.Level == 2)
                {
                    SimpleEvents.Instance.Raise(new InputDownEvent());
                }
                else
                {
                    SimpleEvents.Instance.Raise(new InputUpEvent());
                }

            }
        }

        private void Awake()
        {
            _mover = GameObject.Find("Player").GetComponent<PlayerMover>();
        }
    }
}