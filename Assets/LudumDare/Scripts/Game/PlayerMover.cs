﻿using DG.Tweening;

using System.Collections;
using UnityEngine;

using Sdn.SpriteBoss;
using Sdn.SpriteBoss.Modules;

using LudumDare.Brushes;
using LudumDare.Managers;

namespace LudumDare.Game
{
    public class PlayerMover : SpriteBossModule
    {
        private bool _reloading;

        public float Speed = 100;
        public bool IsSpeedy;

        private PlayerObject _player;
        private int _level;

        public int Level
        {
            get { return _level; }
        }

        public override void OnModuleInit()
        {
            D.Trace("[ObjectMover] OnModuleInit");
            base.OnModuleInit();
        }

        public void MoveLeft()
        {
            D.Trace("[ObjectMover] MoveLeft");
            transform.Translate(Vector2.left * Speed * Time.deltaTime);
            if (transform.position.x < 0)
                transform.position = new Vector2(0, _level * 48);

            if (IsSpeedy)
                GameManager.Instance.PrefabFactory.CreatePrefab("Dust", spriteBoss.transform.position + (Vector3.down * 12.0f));
        }

        public void MoveRight()
        {
            D.Trace("[ObjectMover] MoveRight");
            transform.Translate(Vector2.right * Speed * Time.deltaTime);
            if (transform.position.x > 304)
                transform.position = new Vector2(304, _level * 48);

            if (IsSpeedy)
                GameManager.Instance.PrefabFactory.CreatePrefab("Dust", spriteBoss.transform.position + (Vector3.down * 12.0f));
        }

        public void MoveUp()
        {
            D.Trace("[ObjectMover] MoveUp");

            _level += 1;

            if (_level > 2)
            {
                _level = 2;
                return;
            }

            StartCoroutine(jumpUp());

            Sequence seq0 = DOTween.Sequence()
                .Append(transform.DOMoveY((_level * 48) + 24, 0.25f))
                .Append(transform.DOMoveY(_level * 48, 0.25f));

            GameManager.Instance.SoundController.PlaySound("Jump");
        }

        public void MoveDown()
        {
            D.Trace("[ObjectMover] MoveDown");

            _level -= 1;

            if (_level < 0)
            {
                _level = 0;
                return;
            }

            StartCoroutine(jumpDown());
        }

        public void FireLeft()
        {
            if (_reloading)
                return;
            GameObject go = GameManager.Instance.PrefabFactory.CreatePrefab("Projectile", transform.position);
            go.GetComponent<SpriteBoss>().MoveInDirection(270, 150);
            Destroy(go, 3.0f);
            StartCoroutine(reloading());
        }

        public void FireRight()
        {
            if (_reloading)
                return;
            GameObject go = GameManager.Instance.PrefabFactory.CreatePrefab("Projectile", transform.position);
            go.GetComponent<SpriteBoss>().MoveInDirection(90, 150);
            Destroy(go, 3.0f);
            StartCoroutine(reloading());
        }

        private IEnumerator jumpDown()
        {
            GameManager.Instance.PrefabFactory.CreatePrefab("Dust", spriteBoss.transform.position + (Vector3.down * 12.0f));

            GameManager.Instance.SoundController.PlaySound("Jump");

            Sequence seq0 = DOTween.Sequence()
                .Append(transform.DOMoveY(((_level + 1) * 48) + 24, 0.25f))
                .Append(transform.DOMoveY(_level * 48, 0.25f));

            yield return seq0.WaitForCompletion();

            GameManager.Instance.PrefabFactory.CreatePrefab("Dust", spriteBoss.transform.position + (Vector3.down * 12.0f));

            yield return null;
        }

        private IEnumerator jumpUp()
        {
            GameManager.Instance.PrefabFactory.CreatePrefab("Dust", spriteBoss.transform.position + (Vector3.down * 12.0f));

            GameManager.Instance.SoundController.PlaySound("Jump");

            Sequence seq0 = DOTween.Sequence()
                .Append(transform.DOMoveY((_level * 48) + 24, 0.25f))
                .Append(transform.DOMoveY(_level * 48, 0.25f));

            yield return seq0.WaitForCompletion();

            GameManager.Instance.PrefabFactory.CreatePrefab("Dust", spriteBoss.transform.position + (Vector3.down * 12.0f));

            yield return null;
        }

        private IEnumerator reloading()
        {
            _reloading = true;
            yield return new WaitForSeconds(2.0f);
            _reloading = false;
            yield return null;
        }


        private void Awake()
        {
            D.Trace("[PlayerMover] Awake");
            _player = GetComponent<PlayerObject>();
        }

    }
}