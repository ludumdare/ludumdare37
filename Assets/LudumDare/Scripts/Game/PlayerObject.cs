﻿using UnityEngine;

using Sdn.SpriteBoss;

using LudumDare.Brushes;

namespace LudumDare.Game
{
    public class PlayerObject : MonoBehaviour
    {
        public PathBrush CurrentPath;

        private void Awake()
        {
            D.Trace("[PlayerObject] Awake");
            transform.position = new Vector2(180, 0);
        }
    }
}
