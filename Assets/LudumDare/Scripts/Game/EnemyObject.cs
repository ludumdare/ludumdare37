﻿using UnityEngine;
using System.Collections;

using Sdn.Core;
using Sdn.SpriteBoss;

using LudumDare.Events;

namespace LudumDare.Game
{
    public class EnemyObject : SpawnObject
    {
        public PresentObject Present;

        public float _speed;
        private SpriteBoss _spriteBoss;

        public EnemyObject()
        {
            SpawnType = SpawnTypes.SPAWN_EMENY;
            SpawnId = System.Guid.NewGuid();
        }

        public void StartLeft()
        {
            _spriteBoss.MoveAlongVector(Vector2.left, _speed);
        }

        public void StartRight()
        {
            _spriteBoss.MoveAlongVector(Vector2.right, _speed);
        }

        private void Update()
        {
            if (transform.position.x < -20 || transform.position.x > 340)
            {
                if (Present != null)
                {
                    D.Log("- enemy outside of room - destroy present");
                    SimpleEvents.Instance.Raise(new LostMagicEvent());
                    Present.DestroyPresent();
                }
            }
        }

        private void Awake()
        {
            _spriteBoss = GetComponent<SpriteBoss>();
        }
    }
}