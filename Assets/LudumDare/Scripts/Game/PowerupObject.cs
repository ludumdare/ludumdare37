﻿using UnityEngine;

using Sdn.Core;

using LudumDare.Data;
using LudumDare.Events;
using LudumDare.Managers;

namespace LudumDare.Game
{
    public class PowerupObject : SpawnObject
    {

        public PowerupData _data;

        public PowerupObject()
        {
            SpawnType = SpawnTypes.SPAWN_POWERUP;
            SpawnId = System.Guid.NewGuid();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            D.Trace("[PowerupObject] OnTriggerEnter2D( collision: {0} )", collision);

            if (collision.tag == "Player" || collision.tag == "Projectile")
            {
                D.Detail("- [{0}] was touched", _data.Id);

                //  disable the collider
                GetComponent<Collider2D>().enabled = false;

                //  send the score event
                D.Fine("- sending score [{0}]", _data.Points);

                SimpleEvents.Instance.Raise(new ScoreEvent
                {
                    Points = _data.Points,
                    Animate = false,
                    Multiplier = 1,
                    Position = collision.transform.position,
                    From = _data.Id,
                });

                //  send the powerup event
                D.Fine("- sending powerup [{0}]", _data.Id);
                SimpleEvents.Instance.Raise(new PowerupEvent
                {
                    Data = _data,
                });
            }
        }
    }

}
