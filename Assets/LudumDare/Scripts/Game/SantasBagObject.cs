﻿using UnityEngine;
using System.Collections;

using Sdn.Core;

using LudumDare.Events;
using LudumDare.Managers;

namespace LudumDare.Game
{
    public class SantasBagObject : MonoBehaviour
    {
        private void onPresentCollectedEvent(PresentCollectedEvent e)
        {
            D.Trace("[SantasBagObject] onPresentCollected ( e: {0} )", e);
            float x = 24 * ( (float)GameManager.Instance.GameController.PresentsCollected / (float)GameManager.Instance.GameController.PresentsNeeded);
            transform.FindChild("Fill").transform.localPosition = (Vector3.zero + (Vector3.up * x));
        }

        private void Start()
        {
            GameManager.Instance.GameController.SantasBag = this;
        }

        private void Awake()
        {
            D.Trace("[SantasBagObject] Awake");
            SimpleEvents.Instance.AddListener<PresentCollectedEvent>(onPresentCollectedEvent);
        }

        private void OnDestroy()
        {
            SimpleEvents.Instance.RemoveListener<PresentCollectedEvent>(onPresentCollectedEvent);
        }
    }
}
