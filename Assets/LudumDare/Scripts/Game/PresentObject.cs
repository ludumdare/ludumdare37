﻿using DG.Tweening;

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using Sdn.Core;

using LudumDare.Events;
using LudumDare.Managers;

using Sdn.SpriteBoss;

namespace LudumDare.Game
{
    public class PresentObject : SpawnObject
    {
        public int _points = 25;
        public int _speed;
        public bool _last;

        public List<Sprite> _presentBoxes;
        public List<Sprite> _presentBows;

        private SpriteRenderer _box;
        private SpriteRenderer _bow;

        private SpriteBoss _spriteBoss;
        private bool _stopped;
        private bool _paused;
        private bool _collected;

        //  PUBLIC

        public PresentObject()
        {
            SpawnType = SpawnTypes.SPAWN_PRESENT;
            SpawnId = System.Guid.NewGuid();
        }

        public void StartLeft()
        {
            //  create the present
            wrapPresent();
            _spriteBoss.MoveAlongVector(Vector2.left, _speed);
        }

        public void StartRight()
        {
            //  create the present
            wrapPresent();
            _spriteBoss.MoveAlongVector(Vector2.right, _speed);
        }

        public void DestroyPresent()
        {
            Destroy(gameObject);
        }

        public void Collect()
        {
            collectPresent();
        }

        public void Pause()
        {
            _spriteBoss.Pause();
        }

        public void Resume()
        {
            _spriteBoss.Resume();
        }

        public void Last()
        {
            _last = true;
            StartCoroutine(flash());

        }

        //  PRIVATE

        private void wrapPresent()
        {
            D.Trace("[PresentBox] createPresent");
            pickBox(_box);
            pickBow(_bow);
        }

        private void pickBox(SpriteRenderer renderer)
        {
            D.Trace("[PresentBox] createBox");
            GetComponent<SpriteRenderer>().sprite = _presentBoxes[Random.Range(0, _presentBoxes.Count)];
        }

        private void pickBow(SpriteRenderer renderer)
        {
            D.Trace("[PresentBox] createBow");
            renderer.sprite = _presentBows[Random.Range(0, _presentBows.Count)];
        }

        private void collectPresent()
        {
            if (_collected)
                return;

            _collected = true;
            _spriteBoss.Stop();
            StartCoroutine(runCollection());
        }

        private IEnumerator flash()
        {
            while(true)
            {
                yield return new WaitForSeconds(0.25f);
                GetComponent<SpriteRenderer>().enabled = false;
                _bow.GetComponent<SpriteRenderer>().enabled = false;
                yield return new WaitForSeconds(0.25f);
                GetComponent<SpriteRenderer>().enabled = true;
                _bow.GetComponent<SpriteRenderer>().enabled = true;

            }
        }

        private IEnumerator runCollection()
        {
            Vector2 pos = GameManager.Instance.GameController.SantasBag.transform.position;
            float dist = Vector3.Distance(transform.position, pos);

            Sequence seq0 = DOTween.Sequence()
                .Append(transform.DOMoveY(pos.y + 72, 0.5f))
                .Append(transform.DOMoveY(pos.y, 1.0f));

            Sequence seq1 = DOTween.Sequence()
                .Append(transform.DOMoveX(pos.x , 0.5f))
                .Append(transform.DOMoveX(pos.x, 1.0f));
            
            Sequence seq2 = DOTween.Sequence()
                .Append(transform.DOScale(Vector3.one * 2.0f, 0.5f))
                .Append(transform.DOScale(Vector3.one * 0.5f, 1.0f));

            seq2.Play();
            seq1.Play();
            yield return seq0.WaitForCompletion();

            //transform.DOMoveX(pos.x, 1.0f);
            //transform.DOMoveY(transform.position.y + (dist / 2), 1.0f).SetLoops(1, LoopType.Yoyo);

            SimpleEvents.Instance.Raise(new ScoreEvent
            {
                Points = _points,
                Animate = false,
                Multiplier = 1,
                Position = transform.position,
                From = "Present",
            });

            SimpleEvents.Instance.Raise(new PresentCollectedEvent());

            Destroy(gameObject);

            yield return null;
        }

        //  MONO

        private void Awake()
        {
            D.Trace("[PresentBox] Awake");
            //  add 2 objects  with sprite renderers
            _box = new GameObject().AddComponent<SpriteRenderer>();
            _box.transform.parent = transform;
            _box.transform.localPosition = Vector2.zero;
            _box.sortingLayerName = "Item";
            _bow = new GameObject().AddComponent<SpriteRenderer>();
            _bow.transform.parent = transform;
            _bow.transform.localPosition = Vector2.up * 8;
            _bow.sortingLayerName = "Item";

            //  get spriteboss
            _spriteBoss = GetComponent<SpriteBoss>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            D.Trace("[PresentBox] OnTriggerEnter2D");

            if (collision.tag == "Conveyor")
            {
                D.Log("- end of the conveyor reached");
                _stopped = true;
                _spriteBoss.Stop();
            }

            if (collision.tag == "Present")
            {
                D.Log("- touched another present");
                if (_stopped)
                {
                    GameManager.Instance.PrefabFactory.CreatePrefab("Explode", transform.position);
                    GetComponent<Collider2D>().enabled = false;
                    SimpleEvents.Instance.Raise(new LostMagicEvent());
                    GameManager.Instance.SoundController.PlaySound("Destroy");
                    DestroyPresent();
                }
            }

            if (collision.tag == "Enemy")
            {
                D.Log("- touched enemy");
                if (collision.GetComponent<EnemyObject>().Present == null)
                {
                    GetComponent<Collider2D>().enabled = false;
                    collision.GetComponent<EnemyObject>().Present = this;
                    _spriteBoss.Stop();
                    transform.parent = collision.transform;
                }
            }

            if (collision.tag == "Player")
            {
                D.Log("- player touched me - yay!!!");
                GetComponent<Collider2D>().enabled = false;
                GameManager.Instance.SoundController.PlaySound("Present");
                collectPresent();
            }

        }

        private void OnDestroy()
        {
            if (GameObject.FindObjectsOfType<PresentObject>().Length <= 0)
                SimpleEvents.Instance.Raise(new LevelCompleted());
        }
    }
}

