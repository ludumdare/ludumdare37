﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

using LudumDare.Data;
using LudumDare.Managers;

namespace LudumDare.Scenes
{
    public class Bootstrap : MonoBehaviour
    {
        public GameSettingsData SettingsData;
                
        public bool _autoStart;
        public string _startingSceneName;

        private void Start()
        {
            D.Trace("[Bootstrap] Start");

            GameManager.Instance.GameSettings = SettingsData;            

            if (!_autoStart)
                return;

            if (string.IsNullOrEmpty(_startingSceneName))
            {
                D.Error("- Starting Scene Name is missing", _startingSceneName);
                return;
            }

            if (!Application.CanStreamedLevelBeLoaded(_startingSceneName))
            {
                D.Error("- could not load Scene [{0}]", _startingSceneName);
                return;
            }

            SceneManager.LoadScene(_startingSceneName);
        }
    }
}