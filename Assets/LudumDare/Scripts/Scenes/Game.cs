﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using Sdn.Core;

using LudumDare.Data;
using LudumDare.Controllers;
using LudumDare.Managers;
using LudumDare.Events;
using LudumDare.Game;

namespace LudumDare.Scenes
{

    public class Game : MonoBehaviour
    {
        public GameLevelData GameData;
        
        public GameObject _panelShiftSummary;
        public GameObject _panelShiftNext;
        public GameObject _panelWin;
        public GameObject _panelLose;
        public GameObject _panelGame;
        public GameObject _panelPause;

        private GameController _gameController;
        private LevelData _levelData;
        private bool _paused;

        //  PUBLIC

        public void Resume()
        {
            D.Trace("[Game] resume");

            _paused = false;

            foreach (PresentObject present in GameObject.FindObjectsOfType<PresentObject>())
            {
                present.Resume();
            }

            GameManager.Instance.ConveyorController.Resume();

        }

        public void Pause()
        {
            D.Trace("[Game] pause");
            _paused = true;

            //  pause conveyors
            GameManager.Instance.ConveyorController.Pause();

            //  pause presents
            foreach (PresentObject present in GameObject.FindObjectsOfType<PresentObject>())
            {
                present.Pause();
            }

            //  pause gremlins


        }

        public void HomeButton()
        {
            SceneManager.LoadScene("10-Home");
        }

        public void NextButton()
        {
            startLevel();
        }

        public void SummaryButton()
        {
            nextLevel();
        }

        public void ResumeButton()
        {
            Resume();
        }

        //  PRIVATE

 
        private void onLevelCompleted(LevelCompleted e)
        {
            endLevel();
        }

        private void onPresentCollected(PresentCollectedEvent e)
        {
            _gameController.PresentsCollected += 1;

            if (_gameController.PresentsCollected >= _gameController.PresentsNeeded)
            {
                endLevel();
            }
        }

        private void resetGame()
        {
            D.Trace("[GameController] ResetGame");
            _gameController.Level = 0;
            _gameController.Magic = 20;
        }

        private void startGame()
        {
            D.Trace("[Game] StartGame");
            nextLevel();
        }

        private void endGame()
        {
            D.Trace("[Game] EndGame");
        }

        private void winGame()
        {
            D.Trace("[Game] WinGame");
        }

        private void loseGame()
        {
            D.Trace("[Game] LoseGame");
            stopLevel();
            showPanelLose();
        }

        private void endLevel()
        {
            D.Trace("[Game] EndLevel");

            stopLevel();
            showPanelShiftSummary();
        }

        private void nextLevel()
        {
            D.Trace("[Game] NextLevel");

            hideAllPanels();

            _gameController.TotalPresentsCollected += _gameController.PresentsCollected;

            _gameController.Level += 1;

            if (_gameController.Level > 24)
            {
                showPanelWin();
            }

            _levelData = GameData.Get(_gameController.Level - 1);
            _gameController.PresentsCollected = 0;
            _gameController.PresentsNeeded = _levelData.PresentsNeeded;

            showPanelShiftNext();

        }

        private void startLevel()
        {
            D.Trace("[Game] StartLevel");

            hideAllPanels();
            showPanelGame();

            if (GameManager.Instance.RoomController != null)
            {
                GameManager.Instance.RoomController.InitRoom();
            }

            if (GameManager.Instance.ConveyorController != null)
            {
                GameManager.Instance.ConveyorController.StartConveyors(_levelData);
            }
        }

        private void stopLevel()
        {
            //  stop the conveyor belt
            GameManager.Instance.ConveyorController.StopConveyors();

            //  destroy remaining presents
            foreach (PresentObject present in GameObject.FindObjectsOfType<PresentObject>())
            {
                present.DestroyPresent();
            }

            //  remove all gremlins

            //  remove all powerups
        }

        private void showPanelGame()
        {
            D.Trace("[Game] showPanelGame");
            hideAllPanels();
            showPanel(_panelGame);
        }

        private void showPanelLose()
        {
            D.Trace("[Game] showPanelLose");
            hideAllPanels();
            Text msg = _panelLose.transform.FindChild("$Message").GetComponent<Text>();
            msg.text = string.Format("{0}\npresents\npacked", _gameController.TotalPresentsCollected);
            showPanel(_panelLose);
        }

        private void showPanelWin()
        {
            D.Trace("[Game] showPanelWin");
            hideAllPanels();
            Text msg = _panelWin.transform.FindChild("$Message").GetComponent<Text>();
            msg.text = string.Format("{0}\npresents\npacked", _gameController.TotalPresentsCollected);
            showPanel(_panelWin);
        }

        private void showPanelShiftSummary()
        {
            D.Trace("[Game] showPanelShiftSummary");
            hideAllPanels();
            Text msg = _panelShiftSummary.transform.FindChild("$Message").GetComponent<Text>();
            msg.text = string.Format("{0}/{1}\npresents\npacked", _gameController.PresentsCollected, _gameController.PresentsNeeded);
            showPanel(_panelShiftSummary);
        }

        private void showPanelShiftNext()
        {
            D.Trace("[Game] showPanelShiftNext");
            hideAllPanels();
            Text msg = _panelShiftNext.transform.FindChild("$Message").GetComponent<Text>();
            msg.text = string.Format("shift {0}\npack {1}\npresents", _gameController.Level, _gameController.PresentsNeeded);
            showPanel(_panelShiftNext);
        }

        private void showPanelPause()
        {
            showPanel(_panelPause);
        }

        private void hideAllPanels()
        {
            D.Trace("[Game] hideAllPanels");
            hidePanel(_panelShiftSummary);
            hidePanel(_panelShiftNext);
            hidePanel(_panelWin);
            hidePanel(_panelLose);
            hidePanel(_panelGame);
            hidePanel(_panelPause);
        }

        private void hidePanel(GameObject panel)
        {
            panel.GetComponent<Canvas>().enabled = false;
        }

        private void showPanel(GameObject panel)
        {
            panel.GetComponent<Canvas>().enabled = true;
        }

        private void onLostMagic(LostMagicEvent e)
        {
            D.Trace("[Game] onLostMagic");
            _gameController.Magic -= 1;

            D.Log("- magic left {0}", _gameController.Magic);

            if (_gameController.Magic < 0)
            {
                loseGame();
            }
        }

        private void onPause(PauseEvent e)
        {
            Pause();
        }

        private void onResume(ResumeEvent e)
        {
            Resume();
        }

        //  MONO

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                D.Log("- escape pressed");
                if (_paused)
                {
                    Resume();
                    showPanelGame();
                    GameManager.Instance.InputController.Resume();
                }
                else
                {
                    GameManager.Instance.InputController.Pause();
                    showPanelPause();
                    Pause();
                }
            }
        }

        private void Start()
        {
            D.Trace("[Game] Start");
            _gameController = GameManager.Instance.GameController;
            resetGame();
            startGame();
        }

        private void OnEnable()
        {
            D.Trace("[Game] OnEnable");
            GameManager.Instance.MusicController.PlayMusic("Game", 0.15f);
        }

        private void Awake()
        {
            D.Trace("[Game] Awake");
            SimpleEvents.Instance.AddListener<LevelCompleted>(onLevelCompleted);
            SimpleEvents.Instance.AddListener<LostMagicEvent>(onLostMagic);
            SimpleEvents.Instance.AddListener<PresentCollectedEvent>(onPresentCollected);
            SimpleEvents.Instance.AddListener<PauseEvent>(onPause);
            SimpleEvents.Instance.AddListener<ResumeEvent>(onResume);
        }

        private void OnDestroy()
        {
            SimpleEvents.Instance.RemoveListener<LevelCompleted>(onLevelCompleted);
            SimpleEvents.Instance.RemoveListener<LostMagicEvent>(onLostMagic);
            SimpleEvents.Instance.RemoveListener<PresentCollectedEvent>(onPresentCollected);
        }
    }
}
