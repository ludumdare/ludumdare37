﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using LudumDare.Managers;

using System.Collections;

namespace LudumDare.Scenes
{
    public class Home : MonoBehaviour
    {
        public Text _titleText;

        public void LoadScene(string name)
        {
            SceneManager.LoadScene(name);
        }

        private void Start()
        {
            _titleText.text = GameManager.Instance.GameSettings.Title;
            GameManager.Instance.MusicController.PlayMusic("Title", 0.15f);
        }
    }
}