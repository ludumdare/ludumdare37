﻿using UnityEngine;
using System.Collections;

namespace LudumDare.Data
{
    [System.Serializable]
    public class GameSettingsData : ScriptableObject
    {
        [SerializeField]
        public string Title;

        [SerializeField]
        public string Version;

        [SerializeField]
        public string Copyright;

        [SerializeField]
        public string Message;

        [SerializeField]
        public int Resolution;

    }
}
