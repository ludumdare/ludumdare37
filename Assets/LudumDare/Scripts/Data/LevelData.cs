﻿using UnityEngine;
using System;

namespace LudumDare.Data
{
    [CreateAssetMenu]
    [Serializable]
    public class LevelData
    {
        [SerializeField]
        public int Shift;

        [SerializeField]
        public int MaxConveyors;

        [SerializeField]
        public int SpawnRate;

        [SerializeField]
        public int MaxSpeed;

        [SerializeField]
        public int VariableSpeed;

        [SerializeField]
        public int PresentsNeeded;

        [SerializeField]
        public int Gremlins;

        [SerializeField]
        public int PowerUps;
    }
}
