﻿using UnityEngine;

using System;
using System.Collections.Generic;

using Sdn.Core.Database;

namespace LudumDare.Data
{
    [CreateAssetMenu]
    [Serializable]
    public class GameLevelData : ScriptableDatabase<LevelData>
    {
    }
}