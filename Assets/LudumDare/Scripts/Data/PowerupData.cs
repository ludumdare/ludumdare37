﻿using System;
using UnityEngine;

namespace LudumDare.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class PowerupData : ScriptableObject
    {
        /// <summary>
        /// The name of the Powerup - used in the Gui.
        /// </summary>
        [SerializeField]
        public string Name;

        /// <summary>
        /// The unique id for the Powerup - used by the controller.
        /// </summary>
        [SerializeField]
        public string Id;

        /// <summary>
        /// The number of Points this will add to the Players score.
        /// </summary>
        [SerializeField]
        public int Points;

        /// <summary>
        /// The amount of Time the Powerup will last.
        /// </summary>
        [SerializeField]
        public int Duration;

        public Sprite Icon;

    }
}