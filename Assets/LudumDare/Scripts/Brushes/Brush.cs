﻿using UnityEngine;
using System.Collections;

namespace LudumDare.Brushes
{
    public class Brush : MonoBehaviour
    {
        public virtual void InitBrush()
        {
            D.Trace("[Brush] InitBrush");
        }

        public virtual void StartBrush()
        {
            D.Trace("[Brush] StartBrush");
        }

        public virtual void StopBrush()
        {
            D.Trace("[Brush] StopBrush");
        }
    }
}