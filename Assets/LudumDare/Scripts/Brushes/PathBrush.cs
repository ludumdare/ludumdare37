﻿using UnityEngine;
using System.Collections;

using LudumDare.Managers;
using LudumDare.Controllers;
using LudumDare.Game;

namespace LudumDare.Brushes
{
    public class PathBrush : Brush
    {
        public PathBrush NodeUp;
        public PathBrush NodeDown;
        public PathBrush NodeLeft;
        public PathBrush NodeRight;
        public bool LeftFacing;

        public override void InitBrush()
        {
            D.Trace("[PathBrush] InitBrush");

        }

    }
}
