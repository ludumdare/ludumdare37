﻿using UnityEngine;
using System.Collections;

using LudumDare.Managers;
using LudumDare.Controllers;
using LudumDare.Game;

namespace LudumDare.Brushes
{
    public class ConveyorBrush : Brush
    {
        public bool Left;

        public override void InitBrush()
        {
            D.Trace("[ConveyorBrush] InitBrush");
            GameManager.Instance.ConveyorController.RegisterConveyor(this);
        }

    }
}
