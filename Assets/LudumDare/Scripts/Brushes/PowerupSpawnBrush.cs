﻿using DG.Tweening;

using UnityEngine;
using System.Collections;

using Sdn.Core;
using Sdn.SpriteBoss;

using LudumDare.Controllers;
using LudumDare.Managers;
using LudumDare.Data;

namespace LudumDare.Brushes
{
    public class PowerupSpawnBrush : Brush
    {
        public override void InitBrush()
        {
            D.Trace("[PowerupSpawnBrush] InitBrush");
            GameManager.Instance.ConveyorController.RegisterPowerupSpawner(this);
        }

        public override void StartBrush()
        {
            D.Trace("[PowerupSpawnBrush] StartBrush");
        }

    }
}
