﻿using UnityEngine;
using System.Collections;

using Sdn.Core;
using Sdn.SpriteBoss;

using LudumDare.Controllers;
using LudumDare.Managers;
using LudumDare.Helpers;

namespace LudumDare.Brushes
{
    public class PlayerStartBrush : Brush
    {
        public override void InitBrush()
        {
            D.Trace("[PlayerStartBrush] InitBrush");
        }

        public override void StartBrush()
        {
            D.Trace("[PlayerStartBrush] StartBrush");
        }
    }
}
