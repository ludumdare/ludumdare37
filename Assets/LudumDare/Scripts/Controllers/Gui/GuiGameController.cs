﻿using System.Collections;

using UnityEngine;
using UnityEngine.UI;

using Sdn.Core;

using LudumDare.Managers;
using LudumDare.Events;

namespace LudumDare.Controllers.Gui
{

    public class GuiGameController : MonoBehaviour
    {
        public Text _scoreText;
        public Text _cheaterText;
        public Text _magicText;
        public Text _presentsText;
        public Text _msgText;
        public Text _shiftText;

        private int _score;
        private bool _active;
        private Coroutine _run;

        //  PRIVATE

        private IEnumerator run()
        {
            while(_active)
            {
                yield return StartCoroutine(updateShift());
                yield return StartCoroutine(updatePresents());
                yield return StartCoroutine(updateCheater());
                yield return StartCoroutine(updateScore());
                yield return null;
            }
            yield return null;
        }   

        private IEnumerator updateShift()
        {
            if (GameManager.Instance.GameController != null)
            {
                _shiftText.text = string.Format("shift {0}", GameManager.Instance.GameController.Level);
            }
            yield return null;
        }

        private IEnumerator updatePresents()
        {
            if (GameManager.Instance.GameController != null)
            {
                _presentsText.text = string.Format("{0}/{1}", GameManager.Instance.GameController.PresentsCollected, GameManager.Instance.GameController.PresentsNeeded);
                _magicText.text = string.Format("{0}", GameManager.Instance.GameController.Magic);
            }
            yield return null;
        }

        private IEnumerator updateCheater()
        {
            if (GameManager.Instance.CheaterController != null)
            {
                if (GameManager.Instance.CheaterController._cheatsOn)
                {
                    _cheaterText.text = " - cheater - ";
                }
                else
                {
                    _cheaterText.text = "";
                }
            }
            else
            {
                _cheaterText.text = "";
            }

            yield return null;
        }

        private IEnumerator updateScore()
        {
            if (_score < GameManager.Instance.ScoreController.Score)
                _score += 5;
            _scoreText.text = _score.ToString();
            yield return null;
        }

        //  MONO

        private void Start()
        {
            _active = true;
            _run = StartCoroutine(run());
        }

        private void Awake()
        {
            D.Trace("[GuiController] Awake");
        }

        private void OnDestroy()
        {
        }
    }
}
