﻿using UnityEngine;

using System.Collections;

using Sdn.Core;

using LudumDare.Managers;
using LudumDare.Events.Input;

namespace LudumDare.Controllers
{
    public class InputController : MonoBehaviour
    {
        private bool _cooldown;
        private bool _paused;

        public void Pause()
        {
            _paused = true;
        }

        public void Resume()
        {
            _paused = false;
        }

        private IEnumerator cooldown()
        {
            _cooldown = true;
            yield return new WaitForSeconds(0.5f);
            _cooldown = false;
            yield return null;
        }

        private void LateUpdate()
        {
            if (_paused)
                return;

            if (Input.GetKey(KeyCode.RightArrow))
            {
                D.Detail("- triggered RIGHT move");
                SimpleEvents.Instance.Raise(new InputRightEvent());
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                D.Detail("- triggered LEFT move");
                SimpleEvents.Instance.Raise(new InputLeftEvent());
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                D.Detail("- triggered fire RIGHT");
                SimpleEvents.Instance.Raise(new InputFireEvent
                {
                    FireLeft = false,
                });
            }

            if (Input.GetKeyDown(KeyCode.Z))
            {
                D.Detail("- triggered fire LEFT ");
                SimpleEvents.Instance.Raise(new InputFireEvent
                {
                    FireLeft = true,
                });
            }

            if (_cooldown)
                return;

            if (Input.GetKey(KeyCode.UpArrow))
            {
                D.Detail("- triggered UP move");
                SimpleEvents.Instance.Raise(new InputUpEvent());
                StartCoroutine(cooldown());
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                D.Detail("- triggered DOWN move");
                SimpleEvents.Instance.Raise(new InputDownEvent());
                StartCoroutine(cooldown());
            }
        }


        private void Awake()
        {
            D.Trace("[InputController] Awake");
            GameManager.Instance.InputController = this;
        }
    }
}