﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using Sdn.Core;

using LudumDare.Managers;
using LudumDare.Events;

namespace LudumDare.Controllers
{
    public class ScoreController : MonoBehaviour
    {
        public int _score;
        public float _queueSpeed;
        
        private Queue<ScoreEvent> _scores;
        private bool _active;
        private Coroutine _run;

        public int Score
        {
            get { return _score; }
        }

        private void onScoreEvent(ScoreEvent e)
        {
            D.Trace("[ScoreController] onScoreEvent ( e: {0} )", e);
            _scores.Enqueue(e);
        }

        private IEnumerator run()
        {
            D.Trace("[ScoreController] run");

            while (_active)
            {
                if (_scores.Count > 0)
                {
                    ScoreEvent score = _scores.Dequeue();
                    processScore(score);
                }
                yield return new WaitForSeconds(_queueSpeed);
                yield return null;
            }

            yield return null;
        }

        private void processScore(ScoreEvent score)
        {
            D.Log("[ScoreController] processScore ( score: {0})", score);
            D.Detail("- adding points [{0}] from [{1}]", score.Points, score.From);
            _score += score.Points;

            //  animate the score?
            if (score.Animate)
            {
                //  TODO: animate the score
            }
        }

        private void Start()
        {
            D.Trace("[ScoreController] Start");
            _run = StartCoroutine(run());
        }

        private void Awake()
        {
            D.Trace("[ScoreController] Awake");
            GameManager.Instance.ScoreController = this;
            SimpleEvents.Instance.AddListener<ScoreEvent>(onScoreEvent);

            _scores = new Queue<ScoreEvent>();
            _active = true;
        }

        private void OnDestroy()
        {
            SimpleEvents.Instance.RemoveListener<ScoreEvent>(onScoreEvent);
        }
    }
}
