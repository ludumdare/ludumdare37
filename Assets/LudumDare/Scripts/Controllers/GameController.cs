﻿using UnityEngine;

using UnityEngine.SceneManagement;
using System.Collections;

using LudumDare.Data;
using LudumDare.Events;
using LudumDare.Managers;
using LudumDare.Game;

namespace LudumDare.Controllers
{
    public class GameController : MonoBehaviour
    {
        public PlayerObject Player { get; set; }
        public int Level { get; set; }
        public int PresentsNeeded { get; set; }
        public int PresentsCollected { get; set; }
        public int TotalPresentsCollected { get; set; }
        public int Magic { get; set; }
        public SantasBagObject SantasBag { get; set; }

        private void Start()
        {
            D.Trace("[GameController] Start");

        }

        private void Awake()
        {
            D.Trace("[GameController] Awake");
            GameManager.Instance.GameController = this;
            Player = GameObject.FindObjectOfType<PlayerObject>();
        }
    }
}
