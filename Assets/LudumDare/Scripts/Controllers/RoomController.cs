﻿using UnityEngine;

using System.Collections.Generic;

using LudumDare.Game;
using LudumDare.Managers;
using LudumDare.Brushes;

namespace LudumDare.Controllers
{
    public class RoomController : MonoBehaviour
    {
        private List<Brush> _brushes;

        public List<Brush> GetAllBrushes()
        {
            return _brushes;
        }

        public void InitRoom()
        {
            D.Trace("[RoomController] InitRoom");

            foreach (Brush pb in _brushes)
            {
                pb.GetComponent<SpriteRenderer>().enabled = false;
            }

            foreach (Brush brush in _brushes)
            {
                brush.InitBrush();
            }

            foreach (Brush brush in _brushes)
            {
                brush.StartBrush();
            }
        }

        private void OnEnable()
        {
            D.Trace("[RoomController] OnEnable");
        }

        private void Awake()
        {
            D.Trace("[RoomController] Awake");
            GameManager.Instance.RoomController = this;
            _brushes = new List<Brush>();

            foreach(Brush brush in GameObject.FindObjectsOfType<Brush>())
            {
                _brushes.Add(brush);
            }
        }

    }
}
