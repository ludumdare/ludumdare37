﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

using Sdn.Core;

using LudumDare.Data;
using LudumDare.Game;
using LudumDare.Managers;
using LudumDare.Brushes;
using LudumDare.Events;

namespace LudumDare.Controllers
{
    static class MyExtension
    {
        private static System.Random rng = new System.Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }

    public class ConveyorController : MonoBehaviour
    {
        private List<ConveyorBrush> _conveyors;
        private List<PowerupSpawnBrush> _powerups;
        private Coroutine _run;
        private bool _active;
        private bool _paused;
        private ConveyorBrush _lastConveyor;
        private Queue<string> _spawners;
        private bool _last = false;

        //  PUBLIC

        public void RegisterConveyor(ConveyorBrush conveyor)
        {
            D.Trace("[ConveyorController] RegisterConveyor");
            _conveyors.Add(conveyor);
        }

        public void RegisterPowerupSpawner(PowerupSpawnBrush powerup)
        {
            D.Trace("[ConveyorController] RegisterConveyor");
            _powerups.Add(powerup);

        }

        public void StartConveyors(LevelData level)
        {
            D.Trace("[ConveyorController] StartConveyors");
            _last = false;
            _active = true;
            _run = StartCoroutine(run(level));
        }

        public void StopConveyors()
        {
            _active = false;
            StopCoroutine(_run);
        }

        public void Pause()
        {
            _paused = true;
        }

        public void Resume()
        {
            _paused = false;
        }


        //  PRIVATE

        private List<string> fillSpawner(LevelData level)
        {
            List<string> spawners = new List<string>();

            for(int i = 0; i < level.PresentsNeeded + level.Shift - 10; i++)
            {
                spawners.Add("PRESENT");
            }

            for (int i = 0; i < level.Gremlins; i++)
            {
                spawners.Add("ENEMY");
            }

            for (int i = 0; i < level.PowerUps; i++)
            {
                spawners.Add("POWERUP");
            }

            spawners.Shuffle();

            for(int i = 0; i < 10; i++)
            {
                spawners.Insert(0, "PRESENT");
            }

            //  shuffle list
            return spawners;
        }

        private IEnumerator run(LevelData level)
        {
            D.Trace("[ConveyorController] run");

            List<string> spawners = fillSpawner(level);

            _spawners = new Queue<string>();

            foreach(string s in spawners)
            {
                _spawners.Enqueue(s);
            }

            float spawn_rate = 60.0f / (level.SpawnRate*2);

            D.Log("- conveyor spawn rate {0} for {1}/min", level.SpawnRate, spawn_rate);

            while (_active)
            {
                if (!_paused)
                {
                    spawnItem(level);
                    yield return new WaitForSeconds(spawn_rate); // items per minute
                }
                yield return null;
            }
            yield return null;
        }

        private void spawnItem(LevelData level)
        {
            D.Trace("[ConveyorBelt] spawnItem");

            ConveyorBrush conveyor;

            while (true)
            {
               conveyor = getConveyor(level.MaxConveyors);
                if (conveyor != _lastConveyor)
                    break;
            }

            _lastConveyor = conveyor;

            if (_spawners.Count > 0)
            {
                string spawn = _spawners.Dequeue();

                if (_spawners.Count <= 0)
                    _last = true;

                if (spawn == "PRESENT")
                {
                    spawnPresent(conveyor);
                }

                if (spawn == "ENEMY")
                {
                    spawnEnemy(conveyor);
                }

                if (spawn == "POWERUP")
                {
                    spawnPowerup(conveyor);
                }
            }
            else
            {
                _active = false;
            }

        }

        private void spawnEnemy(ConveyorBrush conveyor)
        {
            D.Trace("[ConveyorBelt] spawnEnemy");
            GameObject enemy = GameManager.Instance.PrefabFactory.CreatePrefab("Enemy");
            enemy.transform.position = conveyor.transform.position;

            //  determine direction

            if (conveyor.Left)
            {
                enemy.transform.GetComponent<EnemyObject>().StartLeft();
            }
            else
            {
                enemy.transform.GetComponent<EnemyObject>().StartRight();
            }
        }

        private void spawnPowerup(ConveyorBrush conveyor)
        {
            D.Trace("[ConveyorBelt] spawnPowerup");

            string[] powerupTypes = { "Stocking", "Magnet", "Star", "Clock", "Candy" };
            string chosen = powerupTypes[Random.Range(0, powerupTypes.Length)] + "Powerup";

            GameObject go = GameManager.Instance.PrefabFactory.CreatePrefab(chosen);

            PowerupSpawnBrush spawn = _powerups[Random.Range(0, _powerups.Count)];
            go.transform.position = spawn.transform.position;

            Destroy(go, 3.0f);
        }

        private void spawnPresent(ConveyorBrush conveyor)
        {
            D.Trace("[ConveyorBelt] spawnPresent");
            GameObject present = GameManager.Instance.PrefabFactory.CreatePrefab("Present");
            present.transform.position = conveyor.transform.position + (Vector3.up * 12.0f);

            if (_last)
                present.transform.GetComponent<PresentObject>().Last();

            //  determine direction

            if (conveyor.Left)
            {
                present.transform.GetComponent<PresentObject>().StartLeft();
            }
            else
            {
                present.transform.GetComponent<PresentObject>().StartRight();
            }
        }

        private ConveyorBrush getConveyor(int active)
        {
            return _conveyors[Random.Range(0, _conveyors.Count)];
        }

        //  MONO

        private void Awake()
        {
            D.Trace("[ConveyorController] Awake");
            GameManager.Instance.ConveyorController = this;
            _conveyors = new List<ConveyorBrush>();
            _powerups = new List<PowerupSpawnBrush>();
        }
    }
}