﻿using UnityEngine;
using System.Collections;

using Sdn.Core;

using LudumDare.Data;
using LudumDare.Managers;
using LudumDare.Events;

namespace LudumDare.Controllers
{
    public class CheatController : MonoBehaviour
    {
        public bool _cheatsOn;
        public int _gotoLevel;

        public PowerupData _stockingPowerupData;
        public PowerupData _magnetPowerupData;
        public PowerupData _starPowerupData;
        public PowerupData _clockPowerupData;
        public PowerupData _candyPowerupData;
        public PowerupData _snowflakPowerupData;

        private void Update()
        {
            if (_cheatsOn)
            {
                //  speed
                if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    D.Trace("[CheatController]");
                    D.Fine("- sending powerup [{0}]", _stockingPowerupData.Id);
                    SimpleEvents.Instance.Raise(new PowerupEvent
                    {
                        Data = _stockingPowerupData
                    });
                }

                //  magnet
                if (Input.GetKeyDown(KeyCode.Alpha2))
                {
                    D.Trace("[CheatController]");
                    D.Fine("- sending powerup [{0}]", _magnetPowerupData.Id);
                    SimpleEvents.Instance.Raise(new PowerupEvent
                    {
                        Data = _magnetPowerupData
                    });
                }

                //  star
                if (Input.GetKeyDown(KeyCode.Alpha3))
                {
                    D.Trace("[CheatController]");
                    D.Fine("- sending powerup [{0}]", _starPowerupData.Id);
                    SimpleEvents.Instance.Raise(new PowerupEvent
                    {
                        Data = _starPowerupData
                    });
                }

                //  clock
                if (Input.GetKeyDown(KeyCode.Alpha4))
                {
                    D.Trace("[CheatController]");
                    D.Fine("- sending powerup [{0}]", _clockPowerupData.Id);
                    SimpleEvents.Instance.Raise(new PowerupEvent
                    {
                        Data = _clockPowerupData
                    });
                }

                //  level completed
                if (Input.GetKeyDown(KeyCode.N))
                {
                    D.Trace("[CheatController]");
                    D.Fine("- sending level completed event");
                    SimpleEvents.Instance.Raise(new LevelCompleted());
                }

                //  level completed
                if (Input.GetKeyDown(KeyCode.G))
                {
                    D.Trace("[CheatController]");
                    D.Fine("- going to level [{0}]", _gotoLevel);
                    GameManager.Instance.GameController.Level = _gotoLevel - 1;
                    SimpleEvents.Instance.Raise(new LevelCompleted());
                }

                //  remove magic
                if (Input.GetKeyDown(KeyCode.Comma))
                {
                    D.Trace("[CheatController]");
                    D.Fine("- removing magic");
                    SimpleEvents.Instance.Raise(new LostMagicEvent());
                    return;
                }

                //  add magic
                if (Input.GetKeyDown(KeyCode.Period))
                {
                    D.Trace("[CheatController]");
                    D.Fine("- adding magic");
                    GameManager.Instance.GameController.Magic += 1;
                }

                //  add present
                if (Input.GetKeyDown(KeyCode.P))
                {
                    D.Trace("[CheatController]");
                    D.Fine("- adding magic");
                    SimpleEvents.Instance.Raise(new PresentCollectedEvent());
                }

            }
        }

        private void Awake()
        {
            GameManager.Instance.CheaterController = this;
        }
    }
}

