﻿using UnityEngine;

using Sdn.Core;

using LudumDare.Data;

namespace LudumDare.Events
{
    public class PowerupEvent : SimpleEvent
    {
        public PowerupData Data { get; set; }
    }

}
