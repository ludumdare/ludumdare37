﻿using UnityEngine;
using System.Collections;

namespace LudumDare.Events.Input
{
    public class InputFireEvent : InputEvent
    {
        public bool FireLeft;
    }
}