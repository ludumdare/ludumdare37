﻿using UnityEngine;

using Sdn.Core;

namespace LudumDare.Events
{
    public class ScoreEvent : SimpleEvent
    {
        public int Points { get; set; }
        public int Multiplier { get; set; }
        public Vector2 Position { get; set; }
        public bool Animate { get; set; }
        public string From { get; set; }

        public override string ToString()
        {
            return string.Format("Points [{0}], Multiplier [{1}], Position [{2}], Animate [{3}], From [{4}]", Points, Multiplier, Position, Animate, From);
        }
    }

}