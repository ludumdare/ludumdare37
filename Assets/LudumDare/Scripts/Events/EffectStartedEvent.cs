﻿using UnityEngine;

using Sdn.Core;

using LudumDare.Data;

namespace LudumDare.Events
{
    public class EffectStartedEvent : SimpleEvent
    {
        public PowerupData Data { get; set; }
    }

}
