﻿using UnityEngine;
using System.Collections;

public class DestroyDelay : MonoBehaviour
{
    public float _destroyDelay;

	void Start ()
    {
        Destroy(gameObject, _destroyDelay);	
	}
}
