﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using LudumDare.Managers;

using System.Collections;

namespace LudumDare.Helpers
{
    public class MenuHelper : MonoBehaviour
    {
        public int baseWidth = 640;
        public int baseHeight = 360;

        public Text _buttonText;

        public void Quit()
        {
            Application.Quit();
        }
        public void ToggleResolution()
        {
            D.Trace("[MenuOptions] ToggleResolution");
            GameManager.Instance.GameSettings.Resolution += 1;

            if (GameManager.Instance.GameSettings.Resolution > 2)
                GameManager.Instance.GameSettings.Resolution = 0;

            setResolution();

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        }

        public void LoadScene(string name)
        {
            SceneManager.LoadScene(name);
        }
        private void setResolution()
        {
            D.Trace("[MenuOptions] setResolution");
            D.Log("- resolution is {0}", GameManager.Instance.GameSettings.Resolution);
            if (GameManager.Instance.GameSettings.Resolution == 0)
            {
                Screen.SetResolution(baseWidth, baseHeight, false);
                _buttonText.text = "2X";
            }

            if (GameManager.Instance.GameSettings.Resolution == 1)
            {
                Screen.SetResolution(baseWidth * 2, baseHeight * 2, false);
                _buttonText.text = "full screen";
            }

            if (GameManager.Instance.GameSettings.Resolution == 2)
            {
                Screen.SetResolution(baseWidth * 3, baseHeight * 3, true);
                _buttonText.text = "1X";
            }
        }

        private void Start()
        {
            D.Trace("[MenuOptions] Start");
            setResolution();
        }

    }
}