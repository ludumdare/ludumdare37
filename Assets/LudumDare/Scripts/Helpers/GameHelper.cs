﻿using UnityEngine;
using System.Collections;

using Sdn.Core;

namespace LudumDare.Helpers
{
    public static class GameHelper 
    {
        /// <summary>
        /// Returns a Point from a Vector
        /// </summary>
        public static  Point VectorToPoint(Vector2 vector)
        {
            int gridw = 16;
            int gridh = 12;

            int xpos = (int)(vector.x);
            int ypos = (int)(vector.y);

            xpos = Mathf.FloorToInt(xpos / gridw);
            ypos = Mathf.FloorToInt(ypos / gridh);

            return new Point(xpos - 1, ypos - 1);
        }
    }
}
