﻿using UnityEngine;
using System.Collections;

public class OnlyOne : MonoBehaviour
{
    private void Awake()
    {
        if (GameObject.Find(name).GetInstanceID() != gameObject.GetInstanceID())
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
