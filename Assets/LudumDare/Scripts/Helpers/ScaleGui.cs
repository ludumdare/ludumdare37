﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using LudumDare.Managers;

public class ScaleGui : MonoBehaviour
{
    private void Start()
    {
        if (Application.isEditor)
            return;

        CanvasScaler cs = GetComponent<CanvasScaler>();
        cs.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
        cs.scaleFactor = (GameManager.Instance.GameSettings.Resolution + 1);
        cs.referencePixelsPerUnit = 100;
    }

}
