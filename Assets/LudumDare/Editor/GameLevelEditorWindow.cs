﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class GameLevelEditorWindow : EditorWindow
{
    private static GameLevelEditorWindow _window;
    private static GameLevelEditor _editor;

    [MenuItem("Game/Level Editor %#s")]
    public static void OpenEditor()
    {
        _window = EditorWindow.GetWindow<GameLevelEditorWindow>();
        _editor = new GameLevelEditor();
        _editor.DataEditorWindow = _window;
    }

    void OnGUI()
    {
        _editor.DrawGui();
    }
}
