﻿using UnityEngine;
using System.Collections;
using Sdn.Core.Data;
using LudumDare.Data;

public class GameLevelEditor : SimpleDataEditor
{
    private GameLevelData _data;

    public GameLevelEditor()
    {
        _data = OpenDatabase<GameLevelData>(@"LudumDare/Data/Levels", @"Levels.asset");
        SetDatabase<GameLevelData>(_data);
        CanAutoSearch = true;
    }
}
